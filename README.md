# PruebaDeConceptos


Prueba de conceptos usando spock para BDD, Spring, Java8 y JPA

[Link de Codelity del ejercicio](https://app.codility.com/programmers/lessons/91-tasks_from_indeed_prime_2016_challenge/rectangle_builder_greater_area)

##### Levantar aplicacion
* ./gradlew build    para buildear y verificar tests
* ./gradlew bootRun  para correr la aplicacion REST


##### Resumen de ejercicio


   * Recurso, recibe arreglo de enteros que indican la longitud de las piezas de corral para armar.
   * Recibe un entero que indica el umbral minimo del area comprendida entre las piezas del corral.
   * Debe ser un rectangulo, por lo que es neceario dos piezas iguales para cada lado del corral.
   * Retorna un arreglo compuesto de strings que indican las dimensiones que cumplan con lo planteado. Ej ["4x2", "3x5"]
   * El corral no puede ser cuadrado, por ej ["4x4"].
   * En el arreglo piezas deben haber minimo 4 piezas.
   * Si en el arreglo piezas cumple el minimo, pero no se puede construir un rectangulo con el umbral minimo, retornar arreglo vacio []</li>
   * Debe cumplir que  1 < umbral < 500
   * Retornar -1 si el resultado excede 100 (originalmente era 1.000.000) resultados.


Halfling Woolly Proudhoof is an eminent sheep herder. He wants to build a pen (enclosure) for his new flock of sheep.
The pen will be rectangular and built from exactly four pieces of fence (so, the pieces of fence forming the opposite
sides of the pen must be of equal length). Woolly can choose these pieces out of N pieces of fence that are stored in
his barn. To hold the entire flock, the area of the pen must be greater than or equal to a given threshold X.


Woolly is interested in the number of different ways in which he can build a pen. Pens are considered different if the
sets of lengths of their sides are different. For example, a pen of size 1×4 is different from a pen of size 2×2
(although both have an area of 4), but pens of sizes 1×2 and 2×1 are considered the same.


Write a function that, given an array A of N integers (containing the lengths of the available pieces of fence) and an integer X,
returns the number of different ways of building a rectangular pen satisfying the above conditions.
The function should return −1 if the result exceeds 100



For example, given X = 5 and the following array A:


  A[0] = 1
  
  
  A[1] = 2
  
  
  A[2] = 5
  
  
  A[3] = 1
  
  
  A[4] = 1
  
  
  A[5] = 2
  
  
  A[6] = 3
  
  
  A[7] = 5
  
  
  A[8] = 1
  
  ![Alt text](https://codility-frontend-prod.s3.amazonaws.com/media/task_static/rectangle_builder_greater_area/static/images/auto/cfd8d68df84af5bf92c26aac46622782.png)
  
  
The function should return 2. The figure above shows available pieces of fence (on the left) and possible
to build pens (on the right). The pens are of sizes 1x5 and 2x5. Pens of sizes 1×1 and 1×2 can be built,
but are too small in area. It is not possible to build pens of sizes 2×3 or 3×5,
as there is only one piece of fence of length 3.
