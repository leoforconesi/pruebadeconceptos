package leo.domain

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import lombok.Data

import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.Temporal
import javax.persistence.TemporalType

@Entity // Indica que esta clase es una entidad de JPA
@EntityListeners(AuditingEntityListener.class)
@Table(name = "usuario")
@Data class Usuario implements Serializable {
    @Id // Permite reconocer id como el ID del objeto
    @GeneratedValue(strategy= GenerationType.AUTO) // @GeneratedValue indica que este ID debe ser generado automaticamente.
    Long id
    String nombre
    String apellido

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    @CreatedDate
    Date createdAt;
    protected Usuario() { }

    Usuario(String nombre, String apellido) {
        this.nombre = nombre
        this.apellido = apellido
    }
//
//    @OneToMany(mappedBy = "parent")
//    private List<UsuarioDatos> datos
}
