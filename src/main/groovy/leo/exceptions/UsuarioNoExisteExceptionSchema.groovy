package leo.exceptions

import lombok.Data

@Data class UsuarioNoExisteExceptionSchema {
    private String message;
    private String details;
    private String hint;
    private String nextActions;
    private String support;

    UsuarioNoExisteExceptionSchema() {
    }

    UsuarioNoExisteExceptionSchema(String message, String details, String hint, String nextActions, String support) {
        this.message = message
        this.details = details
        this.hint = hint
        this.nextActions = nextActions
        this.support = support
    }
}
