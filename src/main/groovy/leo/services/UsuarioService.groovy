package leo.services

import leo.domain.Usuario
import leo.exceptions.UsuarioNoExisteException

interface UsuarioService {

    /**
     *
     * @param usuario
     */
    void storeUser(Usuario usuario)

    /**
     *
     * @param nombre
     * @return
     */
    List<Usuario> findUser(String nombre) throws UsuarioNoExisteException

}