package leo.services

import leo.domain.Usuario
import leo.exceptions.UsuarioNoExisteException
import leo.repositories.UsuariosRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UsuarioServiceImpl implements UsuarioService {
    private static final Logger log = LoggerFactory.getLogger(UsuarioServiceImpl.class)

    @Autowired
    UsuariosRepository usuariosRepository

    @Override
    List<Usuario> findUser(String nombre) throws UsuarioNoExisteException {
        List<Usuario> usuarios = usuariosRepository.findByNombre(nombre)
        if (usuarios.size() > 0) {
            usuarios
        } else {
            log.error("Usuarios con nombre = " + nombre + " no existe")
            throw new UsuarioNoExisteException(
                    "No existe ningun usuario",
                    "No existe ningun usuario, intente con otro nombre",
                    "----",
                    "Consulte a operador para mas info",
                    "http://soporte.com"
            )
        }
    }

    @Override
    void storeUser(Usuario usuario) {
        usuariosRepository.save(usuario)
    }
}
