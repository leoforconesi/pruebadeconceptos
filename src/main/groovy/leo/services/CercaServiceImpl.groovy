package leo.services

import org.springframework.stereotype.Service

import java.util.stream.Collectors

@Service
class CercaServiceImpl implements CercaService {
/**
 *  1- hay mas de 3 elementos (ok)
 *  2- hay una copia de al menos 2 elementos. (ok)
 *  3- el umbral es mayor al resultado de la multiplicacion de la combinacion del contenido de a par de elementos sin repetir
 */
    @Override
    List<String> obtenerCombinacion(Collection<Integer> piezas, int umbral) throws IllegalArgumentException {
        List<String> res = new ArrayList<String>()
        if (piezas.size() > 3 && umbral > 0) {
            HashMap<Integer, Integer> frecuenciaDeElementosRepetidos = verificarCopiaDeDosElementos(piezas)
            if (frecuenciaDeElementosRepetidos.size() >= 2) {
                res = obtenerResultados(umbral, frecuenciaDeElementosRepetidos)
            } else {
                // no hay elementos para formar un rectangulo
            }
        } else {
            throw new IllegalArgumentException("Revisar argumentos pasados")
            // no cumple las condiciones para calcular. Devolver exception
        }
        res
    }
/**
 *
 * @param piezas Coleccion de piezas, cada elemento representa una pieza y su valor el tamaño que tiene
 * @return un mapa indicando la frecuencia de aquellos elementos que se repitan al menos una vez
 */
    private static HashMap<Integer, Integer> verificarCopiaDeDosElementos(Collection<Integer> piezas) {
        HashMap<Integer, Integer> elementosRepetidos = new HashMap<>()
        for (Integer pieza : piezas) {
            if (elementosRepetidos.get(pieza) == null) {
                elementosRepetidos.put(pieza, 1)
            } else {
                elementosRepetidos.put(pieza, elementosRepetidos.get(pieza) + 1)
            }
        }
        return elementosRepetidos
    }

    private static List<String> obtenerResultados(int umbral, HashMap<Integer, Integer> frecuenciaDeElementosRepetidos) {
        List<String> resultado = new ArrayList<>()
        List<Integer> elementos = frecuenciaDeElementosRepetidos.entrySet().stream()
                .filter{entry -> entry.getValue() >= 2}
                .map{it -> it.getKey()}
                .collect(Collectors.toList())
        Collections.sort(elementos)

        List<Integer> temporal = new ArrayList<>()
        for (Integer number : elementos) {
            int factor = (int) Math.ceil((double) umbral / (double) number)
            if (number >= factor) {
                temporal.add(number)
            }
        }

        // Construye respuesta
        for (int i = 0; i < temporal.size(); i++) {
            for (int j = 0; j < temporal.size() && i != j ; j++) {
                resultado.add(temporal.get(i) + "x" + temporal.get(j));
            }
        }
        return resultado
    }
}
