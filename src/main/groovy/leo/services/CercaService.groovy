package leo.services

interface CercaService {
    /**
     *
     * @param piezas arreglo indicando la longitud de cada pieza
     * @param umbral valor minimo del area admitida
     * @return List<String> con pares admitidos. Puede retornar vacio.
     */
    List<String> obtenerCombinacion(Collection<Integer> piezas, int umbral) throws IllegalArgumentException

}