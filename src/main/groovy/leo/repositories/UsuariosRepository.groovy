package leo.repositories

import leo.domain.Usuario
import org.springframework.data.repository.CrudRepository

interface UsuariosRepository extends CrudRepository<Usuario, Long> {
    List<Usuario> findByNombre(String nombre);

    List<Usuario> findByApellido(String apellido);

    Usuario findById(long id);
}