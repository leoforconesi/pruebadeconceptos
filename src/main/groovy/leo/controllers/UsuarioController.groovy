package leo.controllers

import leo.exceptions.UsuarioNoExisteException
import leo.services.UsuarioServiceImpl
import leo.domain.Usuario
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping(produces = 'application/json')
class UsuarioController {

    @Autowired
    private UsuarioServiceImpl service

    @GetMapping('/usuarios')
    ResponseEntity<Collection<Usuario>> getAllUsers() {
        new ResponseEntity<>(service.findAll(), HttpStatus.OK)
    }

    @GetMapping('/usuario/{name}')
    ResponseEntity<List<Usuario>> helloByName(@PathVariable String name) {
        try {
            new ResponseEntity<>(service.findUser(name), HttpStatus.OK)
        } catch(UsuarioNoExisteException e) {
            new ResponseEntity<>(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping(path ='/usuario', consumes = "application/json", produces = "application/json" )
    ResponseEntity<String> saveUser(@RequestBody Usuario usuario) {
        service.storeUser(usuario)
        new ResponseEntity<String>("Usuario guardado", HttpStatus.OK)
    }
}
