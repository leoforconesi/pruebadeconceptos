package leo.services

import leo.IntegrationTestConfiguration
import leo.domain.Usuario
import leo.repositories.UsuariosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Import
import org.springframework.context.support.ClassPathXmlApplicationContext
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@SpringBootTest(webEnvironment = RANDOM_PORT)
@Import(IntegrationTestConfiguration)
class UsuarioServiceSpec extends Specification{

    @Autowired
    UsuarioService usuarioService

    // TODO esto no esta guardando en la base en memoria, por el momento los datos de la base local estan comentados
    void 'grabar un usuario'() {
        given: 'se graba un usuario'
            Usuario usuario = new Usuario("Leo", "Forconesi")
            usuarioService.storeUser(usuario)
        when: 'Lee usuarios de la base de datos'
            List<Usuario> resultado = usuarioService.findUser("Leo")
        then: "La base de datos contiene el valor cargado"
        resultado.size() >= 1
//            resultado.size() == 1
    }

    void 'Buscar usuario'() {

    }
}

