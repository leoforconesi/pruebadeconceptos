package leo.services

import leo.IntegrationTestConfiguration
import leo.domain.Usuario
import leo.services.CercaService

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.context.annotation.Import
import spock.lang.Shared
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

// Specification Resumido:
// Recurso, recibe arreglo de enteros que indican la longitud de las piezas de corral para armar.
// Recibe un entero que indica el umbral minimo del area comprendida entre las piezas del corral.
// Debe ser un rectangulo, por lo que es neceario dos piezas iguales para cada lado del corral.
// Retorna un arreglo compuesto de strings que indican las dimensiones que cumplan con lo planteado. Ej ["4x2", "3x5"]
// El corral no puede ser cuadrado, por ej ["4x4"].
// En el arreglo piezas deben haber minimo 4 piezas.
// Si en el arreglo piezas cumple el minimo, pero no se puede construir un rectangulo con el umbral minimo, retornar arreglo vacio []
// Debe cumplir que  1 < umbral < 500
// Retornar -1 si el resultado excede 100 (originalmente era 1.000.000) resultados.

// Esta clase define una "Specification"
@SpringBootTest(webEnvironment = RANDOM_PORT)
@Import(IntegrationTestConfiguration)
class CercaServicioSpec extends Specification {

    @Autowired
    TestRestTemplate restTemplate

    @Autowired
    CercaService cercaService

    // @Shared
    // Sometimes you need to share an object between feature methods.
    // For example, the object might be very expensive to create,
    // or you might want your feature methods to interact with each other
    // To achieve this, declare a @Shared field
    @Shared usuario = new Usuario("Leo", "Forconesi")

    //Feature methods are the heart of a specification.
    //They describe the features (properties, aspects) that you expect to find in the system under specification.
    //By convention, feature methods are named with String literals.
    //Try to choose good names for your feature methods, and feel free to use any characters you like!
    //Conceptually, a feature method consists of four phases:
    //  1 - Set up the feature’s fixture
    //  2 - Provide a stimulus to the system under specification
    //  3 - Describe the response expected from the system
    //  4 - Clean up the feature’s fixture
    void 'devuelve 1 resultado correcto'() {
        given: 'Un arreglo de piezas donde solo hay un par correcto de elementos'
            Collection<Integer> piezas = [3, 3, 4, 4, 1, 1, 2, 9]
            int umbral = 5
        when: 'Se llama el metodo para obtener resultado'
            ArrayList<String> res = cercaService.obtenerCombinacion(piezas, umbral)
        then: 'El unico par correcto es de 4x3'
            res.get(0) == "4x3"
    }

    void 'validar piezas arreglo malo'() {
        given: 'Un arreglo de piezas menor que el minimo admitido'
            Collection<Integer> piezas = [3, 7]
            int umbral = 5
        when: 'Un arreglo de piezas menor que el minimo admitido'
            ArrayList<String> res = cercaService.obtenerCombinacion(piezas, umbral)
        then: 'Arroja Illegal argument exception indicando que el arreglo es incorrecto'
            IllegalArgumentException ex = thrown()
        // TODO implementar custom exceptions
            ex.message == 'Revisar argumentos pasados'
    }

    void 'validar piezas arreglo vacio'() {

    }

    void 'validar umbral minimo ok'() {

    }

    void 'validar umbral minimo menor que 1'() {
        given: 'Un arreglo de piezas'
            Collection<Integer> piezas = [3, 3, 4, 4, 1, 1, 2, 9]
        and:  'un umbral negativo'
            int umbral = -2
        when: 'se trata de obtener resultados'
            ArrayList<String> res = cercaService.obtenerCombinacion(piezas, umbral)
        then: 'Arroja Illegal argument exception indicando que el umbral es incorrecto'
            IllegalArgumentException ex = thrown()
            ex.message == 'Revisar argumentos pasados'
    }

    void 'validar umbral minimo mayor que 500'() {

    }

    void 'validar mas de 1,000,000,000 resultados'() {

    }

    void 'no hay resultados'() {

    }

    void 'devuelve el maximo permitido de resultados mas uno esperado -1'() {

    }
}
